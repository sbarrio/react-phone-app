let router = require("express").Router();

// Default api root response
router.get("/", function (req, res) {
    res.json({
        status : "Phone API - v1.0",
        message: "Ready",
    })
});

// Device routes
var deviceController = require("../controller/deviceController");

router.route("/phones")
    .get(deviceController.index);

router.route("/phones/:device_id")
    .get(deviceController.view);

// More route controllers...    

module.exports = router;