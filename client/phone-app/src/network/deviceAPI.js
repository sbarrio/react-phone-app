import Request from './request';

const API_ENDPOINT = "http://localhost:8080/api/";

const GET_DEVICES = "phones";
const GET_DEVICE_BY_ID = "phones/:device_id";

class DeviceAPI {

    static getDevices() {

        let url = API_ENDPOINT + GET_DEVICES;
        return Request.get(url);

    }

    static getDeviceById(deviceId) {

        let url = API_ENDPOINT + GET_DEVICE_BY_ID.replace(":device_id", deviceId);
        return Request.get(url);

    }

}

export default DeviceAPI;