import DeviceAPI from '../network/deviceAPI';

class DeviceService {

    static getDevices() {

        return DeviceAPI.getDevices();

    }

    static getDeviceById(deviceId) {

        return DeviceAPI.getDeviceById(deviceId);

    }

}

export default DeviceService;