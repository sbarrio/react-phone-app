// Device controller.js

Device = require("../model/deviceModel");

const STATUS_SUCCESS = "success";
const STATUS_ERROR = "error";

exports.index = function(req, res) {

    Device.get(function (err, devices) {

        if (err) {

            res.json({
                status: STATUS_ERROR,
                message: err,
            });

        } else {

            res.json({
                status: STATUS_SUCCESS,
                data: devices,
            });

        }

    });

};

exports.view = function(req, res) {

    Device.findById(req.params.device_id, function(err, device) {

        if (err){

            res.json({
                status: STATUS_ERROR,
                message: err,
            });

        } else {

            res.json({
                status: STATUS_SUCCESS,
                data: device,
            });

        }

    });

}