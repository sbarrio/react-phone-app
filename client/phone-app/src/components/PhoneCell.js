import React, { Component } from 'react';

import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions';

class PhoneCell extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
              phone : this.props.phone,
        }
    
      }
    
      componentDidMount() {
    
      }
    
      componentDidUpdate() {
    
      }

      showDetail() {

            // Show up the phone's detail screen
            this.props.setCurrentDevice(this.props.phone);
            this.props.setIsDetailShowing(true);

      }

      render() {

            if (this.state.phone != null) {

                return (

                    <div className="PhoneCell" 
                          key={this.state.phone.id} 
                          onClick={() => this.showDetail()}
                          >

                            <img className="PhoneCellImage" src={this.state.phone.img_url} alt={this.state.phone.name}/>
    
                            <div className="PhoneCellTitle">
                                {this.state.phone.name}
                            </div>
    
                    </div>
                )

            }

            return null;

      }

}

function mapStateToProps(state, props) {
    return {
      currentDevice   :   state.uiReducer.currentDevice,
      isDetailShowing :   state.uiReducer.isDetailShowing,
    }
}
  
function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}
  
export default connect(mapStateToProps, mapDispatchToProps)(PhoneCell);





