import { DEVICES_AVAILABLE } from '../actions/';

let devicesState = { devices: null };

export const devicesReducer = (state = devicesState, action) => {

    switch (action.type) {
        case DEVICES_AVAILABLE:
            state = Object.assign({}, state, { devices: action.devices });
            return state;
        default:
            return state;
    }
    
};