import React, { Component } from 'react';
import PhoneCell from "../components/PhoneCell";

class PhoneListContainer extends Component {


    constructor(props) {
        super(props);

        this.state = {
            phones : this.props.phones,
        }
    }

    componentDidMount() {

    }

    componentDidUpdate() {

    }

    renderPhoneCell(phone) {

        return(<PhoneCell phone={phone} key={phone.id}/>);

    }

    render() {

        return (

            <div className="PhoneListContainer">

                {this.state.phones != null && this.state.phones.map((p) => {
                        return this.renderPhoneCell(p);
                    })
                }

            </div>

        );

    }

}

export default PhoneListContainer;