//Phones API

let express = require('express');

//Useful for local testing
var cors = require('cors')

let app = express();
app.use(cors());

//Load up our API routes
let apiRoutes = require("./routes/api-routes");
app.use("/api",apiRoutes);

// Set up root uri message
app.get("/", (req, res) => res.send('Phone API'));

//Start up the service
var port = process.env.PORT || 8080;
app.listen(port, function() {
    console.log("Running Phone API on port " + port);
});