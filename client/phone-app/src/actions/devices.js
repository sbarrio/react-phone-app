import DeviceService from "../services/deviceService";

export const DEVICES_AVAILABLE = "DEVICES_AVAILABLE";

export function getDevices() {    

    return (dispatch, getState) => {

        return DeviceService.getDevices().then((devices) => {

            dispatch({type: DEVICES_AVAILABLE, devices: devices.data.data});

        }).catch((error) => {

            console.log(error);

        });
    }

}