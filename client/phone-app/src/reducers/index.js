import { combineReducers } from 'redux';
import { devicesReducer } from './devices';
import { uiReducer } from './ui';

// Combine all the reducers
const rootReducer = combineReducers({
    devicesReducer,
    uiReducer,
    // ,[ANOTHER REDUCER], [ANOTHER REDUCER] ....
})

export default rootReducer;