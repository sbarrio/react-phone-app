import React, { Component } from 'react';

import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../actions';

class PhoneDetailComponent extends Component {


    constructor(props) {
        super(props);

        this.state = {
            phone : this.props.phone,
        }
    }

    componentDidMount() {

    }

    componentDidUpdate() {

    }

    closeDetail() {

        // Close the device's detail screen
        this.props.setCurrentDevice(null);
        this.props.setIsDetailShowing(false);

    }

    render() {

        return (

            <div className="PhoneDetailComponent">

                <div className="PhoneDetailHeader">

                    <div className="PhoneDetailCloseButton" onClick={() => this.closeDetail()}>
                        &times;
                    </div>

                    <div className="PhoneDetailTitle">
                        {this.state.phone.name}
                    </div>

                </div>

                <div className="PhoneDetailContent">

                    <img className="PhoneDetailImage" 
                        src={this.state.phone.img_url} 
                        alt={this.state.phone.name}/>

                    <div className="PhoneDetailSpecListHolder">

                        <ul className="PhoneDetailSpecList">

                            <li className="PhoneDetailSpecListItem">
                                <b>CPU:</b> {this.state.phone.cpu}
                            </li>

                            <li className="PhoneDetailSpecListItem">
                                <b>RAM:</b> {this.state.phone.ram}    
                            </li>

                            <li className="PhoneDetailSpecListItem">
                                <b>Resolution:</b> {this.state.phone.resolution}    
                            </li>

                            <li className="PhoneDetailSpecListItem">
                                <b>Battery:</b> {this.state.phone.battery}    
                            </li>

                            <li className="PhoneDetailSpecListItem">
                                <b>OS:</b> {this.state.phone.os}    
                            </li>

                            <li className="PhoneDetailSpecListItem">
                                <b>Price:</b> {this.state.phone.price} {this.state.phone.currency}     
                            </li>

                            <li className="PhoneDetailSpecListItem">
                                <b> Description</b>: {this.state.phone.description}    
                            </li>

                        </ul>

                    </div>
                    

                </div>
                
            </div>

        );

    }

}

function mapStateToProps(state, props) {
    return {
      currentDevice   :   state.uiReducer.currentDevice,
      isDetailShowing :   state.uiReducer.isDetailShowing,
    }
}
  
function mapDispatchToProps(dispatch) {
    return bindActionCreators(Actions, dispatch);
}
  
export default connect(mapStateToProps, mapDispatchToProps)(PhoneDetailComponent);