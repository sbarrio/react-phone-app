
import axios from 'axios';

class Request {

    //Basic networking
    static get(url) {

        return axios.get(url).then((response) => {

            return response;

        }).catch((error) => {

            console.log(error);
            return error;

        })

    }

    static post(url, params) {

        return axios.post(url, params).then((response) => {

            return response;

        }).catch((error) => {

            console.log(error);
            return error;

        })

    }

}

export default Request;

