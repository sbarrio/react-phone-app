import { createStore, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { createLogger } from 'redux-logger';
import rootReducer from '../reducers/index'; //Import the reducer


//Configure and create logger
const LOGGER_ON = false;
const logger = createLogger({predicate: (getState, action) => LOGGER_ON})

// Connect our store to the reducers
export default function configureStore(initialState={}) {
    return createStore(
     rootReducer,
        applyMiddleware(thunk, logger)
    );
}