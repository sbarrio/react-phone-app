//deviceModel.js

const DELAY_RESPONSE = false;        // If active, all responses are sent back after DELAY_RESPONSE_TIME has passed (useful to mock slow network connections)
const DELAY_RESPONSE_TIME = 3000;   // in ms

let deviceList = [
    {
        id: 1,
        name: "Google Pixel 2",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        price: "550",
        currency:"€",
        resolution: "1080 x 1920",
        battery: "2700mAh",
        cpu: "Qualcomm Snapdragon 835",
        ram: "4GB",
        os: "Android 8",
        img_url: "https://cdn2.gsmarena.com/vv/bigpic/google-pixel-2.jpg", 
    },
    {
        id: 2,
        name: "iPhone XS",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        price: "1150",
        currency:"€",
        resolution: "1125 x 2436",
        battery: "2658mAh",
        cpu: "Apple A12",
        ram: "4GB",
        os: "iOS 12",
        img_url: "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-xs-new.jpg", 
    },
    {
        id: 3,
        name: "Samsung Galaxy S9",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        price: "490",
        currency:"€",
        resolution: "1440 x 2960",
        battery: "3000mAh",
        cpu: "Exynos 9810 Octa",
        ram: "4GB",
        os: "Android 8",
        img_url: "https://cdn2.gsmarena.com/vv/bigpic/samsung-galaxy-s9-.jpg", 
    },
    {
        id: 4,
        name: "Xiaomi Mi 8",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        price: "380",
        currency:"€",
        resolution: "1080 x 2248",
        battery: "3400mAh",
        cpu: "Qualcomm Snapdragon 845",
        ram: "8GB",
        os: "Android 8.1",
        img_url: "https://cdn2.gsmarena.com/vv/bigpic/xiaomi-mi8.jpg", 
    },
    {
        id: 5,
        name: "iPhone SE",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        price: "300",
        currency:"€",
        resolution: "640 x 1136",
        battery: "1624mAh",
        cpu: "Apple A9",
        ram: "2GB",
        os: "iOS 12",
        img_url: "https://cdn2.gsmarena.com/vv/bigpic/apple-iphone-5se-ofic.jpg", 
    },
    {
        id: 6,
        name: "Blackberry KEY2 LE",
        description: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        price: "330",
        currency:"€",
        resolution: "1080 x 1620",
        battery: "3000mAh",
        cpu: "Qualcomm Snapdragon 636",
        ram: "4GB",
        os: "Android 8.1",
        img_url: "https://cdn2.gsmarena.com/vv/bigpic/blackberry-key2-le-.jpg", 
    },
]

let Device = {};

Device.get = function(callback) {

    processCallback(() => {callback(null, deviceList)});         

};

Device.findById = function(device_id, callback) {

    if (device_id) {

        for (d in deviceList) {

            if (deviceList[d].id == device_id) {

                processCallback(() => {callback(null, deviceList[d])});                
                return;

            }
            
        }

    }

    callback({"error": "No device found for device_id: " + device_id}, null);

}

function processCallback(callback) {

    DELAY_RESPONSE ? 
        delayedCallback(DELAY_RESPONSE_TIME, () => {callback()}) 
        :
        callback();

}

function delayedCallback(delay, callback) {

    setTimeout(function() {
        callback();
    }, delay);

}

module.exports = Device;