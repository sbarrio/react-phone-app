#Phone App (server/client) - sbarrio 2018 v1.0


##Server - PhoneAPI 

This is a basic Node.js server app that provides and endpoint from which users can retrieve a list of smartphones.

### Running

Go to the main react-phone-app/server/apitest/ test directory and run: 

***npm install***

(Note: You may get a warning concerning the repository field inside package.json, this is not an issue and you can ignore it)

And then:

***node index.js***

You should now be able to access the API through http://localhost:8080/api

### Mocking network latency

Since this API runs with mocked data and doesn't access any data endpoint it may be useful to be able to mock up delayed network request.

To do so, you can turn on a built-in delay system by configuring the following values inside deviceModel.js:

***const DELAY_RESPONSE = true;***

***const DELAY_RESPONSE_TIME = 3000; // in ms***






##Client - PhoneApp 

This is a basic React.js app that consumes a simple test API that returns a list of Smartphones.

You can browse around the phone list and tap on each one to see details about it.

### Running

Go to the main react-phone-app/client/phone-app/ directory and run: 

***npm install***

And then:

***npm start***

Make sure the API project is running on http://localhost:8080, or if you deploy it remotely change the API_ENDPOINT constant inside this project's deviceAPI.js to match accordingly.