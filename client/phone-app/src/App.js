import React, { Component } from 'react';
import './App.css';

import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as Actions from './actions';

import { Levels } from 'react-activity';
import 'react-activity/dist/react-activity.css';

import PhoneListContainer from "./components/PhoneListContainer";
import PhoneDetailComponent from "./components/PhoneDetailComponent";

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
          devices : null,
          currentDevice : this.props.currentDevice,
          isDeviceDetailShowing : this.props.isDetailShowing,
    }

  }

  componentDidMount() {

    // Device list initial load
    this.props.getDevices().then(() => {
      this.setState({
        devices : this.props.devices,
      })
    })

  }

  componentDidUpdate() {

  }

  componentWillReceiveProps(props) {

    // Update current state when changes happen on shared redux store
    this.setState({
      currentDevice: props.currentDevice,
      isDeviceDetailShowing: props.isDetailShowing,
    })

  }

  render() {

    return (

      <div className="App">

        <header className="App-header">

          <div className="HeaderTitle">
            Phones
          </div>

        </header>

        <div className="App-content">

            <div>

                {this.state.devices == null && 

                  <div className="ActivityIndicatorHolder">

                    <Levels/>

                  </div>

                }

                {this.state.devices != null &&

                  <PhoneListContainer phones={this.state.devices}/>

                }

            </div>

        </div>


        {this.state.isDeviceDetailShowing &&

           <PhoneDetailComponent phone={this.state.currentDevice}/>

        }

        <footer className="App-footer">

          <a className="FooterTitle" href="http://sbarrio.info">
            sbarrio - 2018
          </a>
        
        </footer>

      </div>

    );

  }

}


function mapStateToProps(state, props) {
  return {
    devices         :   state.devicesReducer.devices,
    currentDevice   :   state.uiReducer.currentDevice,
    isDetailShowing :   state.uiReducer.isDetailShowing,
  }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(Actions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
