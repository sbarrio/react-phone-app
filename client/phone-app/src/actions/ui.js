export const CURRENT_DEVICE_UPDATED = "CURRENT_DEVICE_UPDATED";
export const DETAIL_SHOWING_UPDATED = "DETAIL_SHOWING_UPDATED";

export function setCurrentDevice(device) {

    return (dispatch, getState) => {
        dispatch({type: CURRENT_DEVICE_UPDATED, currentDevice: device});
    }

}

export function setIsDetailShowing(isShowing) {

    return (dispatch, getState) => {
        dispatch({type: DETAIL_SHOWING_UPDATED, isDetailShowing: isShowing});
    }

}