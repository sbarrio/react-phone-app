
import { CURRENT_DEVICE_UPDATED, DETAIL_SHOWING_UPDATED } from '../actions/';

let uiState = {currentDevice: null, isDetailShowing : false};

export const uiReducer = (state = uiState, action) => {

    switch(action.type) {
        case CURRENT_DEVICE_UPDATED:
            state = Object.assign({}, state, { currentDevice: action.currentDevice });
            return state;
        case DETAIL_SHOWING_UPDATED:
            state = Object.assign({}, state, { isDetailShowing: action.isDetailShowing });
            return state;
        default: 
            return state;
    }

}